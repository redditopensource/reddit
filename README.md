# reddit

<img src="https://secure.gravatar.com/avatar/c638493729c2f009988c9e5bd9b5e116?s=200" align="right">

Greetings!

This is the primary codebase that powers [reddit.com](https://old.reddit.com).

For notices about major changes and general discussion of reddit development, subscribe to the [/r/redditdev](https://old.reddit.com/r/redditdev) and [/r/changelog](https://old.reddit.com/r/changelog) subreddits. 

You can also chat with us via IRC in [#reddit-dev on freenode](http://webchat.freenode.net/?channels=reddit-dev).

---

### Quickstart

To set up your own instance of reddit to develop with, we have a handy [install script for Ubuntu](https://gitlab.com/redditopensource/reddit/wikis/reddit-install-script-for-Ubuntu) that will automatically install and configure most of the stack.

Alternatively, refer to our [Install Guide](https://gitlab.com/redditopensource/reddit/wikis/Install-guide) for instructions on setting up reddit from scratch. Many frequently asked questions regarding local reddit installs are covered in our [FAQ](https://gitlab.com/redditopensource/reddit/wikis/FAQ).

### APIs

To learn more about reddit's API, check out our [automated API documentation](https://old.reddit.com/dev/api) and the [API wiki page](https://gitlab.com/redditopensource/reddit/wikis/API). Please use a unique User-Agent string and take care to abide by our [API rules](https://gitlab.com/redditopensource/reddit/wikis/API#wiki-rules).

Happy hacking!

### Issues and Contribution Guidelines

Thanks for wanting to help make reddit better! First things first, though: **github issues is only for _confirmed_, active bugs**. Please submit ideas to [/r/ideasfortheadmins](https://old.reddit.com/r/ideasfortheadmins/).

Please read more on contributions in [CONTRIBUTING.md](CONTRIBUTING.md).
