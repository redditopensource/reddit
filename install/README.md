# Reddit installer development instructions

This folder contains all of the installation scripts required to build reddit on a stock Ubuntu 14.04 (trusty) box.  Originally all of this was included in `../install-reddit.sh` but the need to fork the image into an base installer for testing as well as a full installer for local use meant some reorganization and fracturing of the original script.

**NOTE** if you find yourself adding files to this folder, please update `$NEEDED` in `../install-reddit.sh` to reflect the addition.  
